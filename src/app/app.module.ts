import { AgmCoreModule } from '@agm/core';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { MapsTrackModule } from 'projects/ng-movup/src/lib/maps-track/maps-track.module';
import {AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    MapsTrackModule.forRoot(environment),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCofjsYXV1QPETBV4bKJgyWiZrCAV2U1Uo'
    }),
    HttpClientModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [AppComponent],
  exports: [],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
