import { TestBed } from '@angular/core/testing';

import { PositionTrackService } from './position-track.service';

describe('PositionTrackService', () => {
  let service: PositionTrackService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PositionTrackService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
