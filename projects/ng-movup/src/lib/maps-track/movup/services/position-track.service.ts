import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PositionTrackService {

  constructor(private http:HttpClient, @Inject('env') private env) { }

    newPoint(params:any): Observable<any>{
      var param = encodeURIComponent( JSON.stringify(params))
      return this.http.get(this.env.map+'/geolocation?param='+param)
    }
    newPointinformation(trip:any,plate:any,account:any): Observable<any>{
      var param = encodeURIComponent( JSON.stringify(trip))
      return this.http.get(this.env.map+'/data_vehicle_detail/'+account+'/'+plate+'?param='+param)
    }

}

