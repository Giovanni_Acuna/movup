export interface position {
  lat: number,
  lng: number,
  transport_id: string,
  connection_alpha: number,
  alert?: number,
  trip?: string,
  color: string
  path?: string
}

export interface data_position {
  transport_id: string,
  transport_code: string,
  provider: string,
  last_report: string,
  owned_by: string,
  origin: string,
  destiny: string,
  trip_id: string,
  trip_code: string,
  time_in_route: string,
  dispatches_visited: number,
  dispatches_total: number,
  alert: number
}
