import { Component, OnInit } from '@angular/core';
import { position } from "../../movup/interfaces/position";
import { PositionTrackService } from '../../movup/services/position-track.service';
import { data_position } from "../../movup/interfaces/position";


declare const google: any;
declare const google_map_extends: any;


@Component({
  selector: 'maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.scss']
})
export class MapsComponent implements OnInit {

  positions: position []
  info_marker:data_position []
  title = 'movup';
  zoom: number = 5;
  latitude: number = 31.74671;// 16.6764464;
  longitude: number = -106.43337;// 82.9687686;

  map:any;
  map_bounds:any;

  constructor(private positionTrackService: PositionTrackService) {
    /*this.positionTrackService.newPointinformation(
      {"trip":"98AK6Y"},
      {"plate":"LH GDL MTY NLD 2720"},
      {"account_id":3001}).subscribe(infomarker => {this.info_marker = infomarker
      console.log('1aca')
      console.log(this.info_marker)})
      console.log('2aca2')*/
  }

  marker_data(trans,tripid){
    this.positionTrackService.newPointinformation(
      trans,
      tripid,
      {"account_id":3001}).subscribe(infomarker => {this.info_marker = infomarker
      console.log(this.info_marker)
      console.log('2aca2')
        return this.info_marker
    })
   }

  ngOnInit(): void {}

  onMapReady(map){
    this.map = map;
    this.map_bounds = new google.maps.LatLngBounds();

    this.positionTrackService.newPoint({"account_id":3001}).subscribe(data => {
    this.positions = data
    console.log(this.positions)
     
    for (let index = 0; index < this.positions.length; index++) {
      const point = this.positions[index];
      this.draw(point)
    }
    });
  }

  private draw(point : any){
    var p = new google.maps.LatLng(point.lat,point.lng);
    var color = "#5E5E5E";
    var opacity = point.connection_alpha;
  
  if ('color' in point && point.color != null){
    color = point.color
  }

  var dot = new google.maps.Marker({
    position: p,
    icon: {
      path: "M-20,0a20,20 0 1,0 40,0a20,20 0 1,0 -40,0",//google.maps.SymbolPath.CIRCLE,
      fillColor: color,
      scale: 0.35,
      strokeColor: "#ffffff",
      //radio: 15,
      strokeWeight: 2,
      fillOpacity: opacity
    },
    draggable: false,
    map: this.map
  });

  var map = this.map;
  dot['dot_data']={lat:point.lat, lng:point.lng, id_transport:point.transport_id, trip_id:point.trip}
  //console.log(dot['dot_data'])

  var dot_infowindow = null;
  var trans: any = null;
  var tripid: any = null;

 
  dot.addListener('click', function(ev:any){
    console.log(dot['dot_data'])
    console.log("clicked element");
    console.log(dot['dot_data'].id_transport)
    console.log(dot['dot_data'].trip_id)

    trans = dot['dot_data'].id_transport;
    tripid = dot['dot_data'].trip_id;
    
    console.log(trans)
    console.log(tripid)

    this.marker_data(trans,tripid)
    /*
    this.positionTrackService.newPointinformation(
      trans,
      tripid,
      {"account_id":3001}).subscribe(infomarker => {this.info_marker = infomarker
      console.log(this.info_marker)
        return this.info_marker
    })
    */

    //marker_data(trans,tripid)
/*
    this.PositionTrackService.newPointinformation(
    {"trip":this.trans},
    {"plate":this.tripid},
    {"account_id":3001}).subscribe(infomarker => {this.info_marker = infomarker
    console.log(this.info_marker)})
*/
    console.log(ev)
    console.log('aca')
    console.log(dot)
    var msg = '<p><strong>latitud : </strong>'+this['dot_data'].lat+'</p>'
        msg += '<p><strong>longitude : </strong>' + this['dot_data'].lng + '</p>'
        msg += '<p><strong>ID trip : </strong>'+this['dot_data'].id_transport+'</p>'; 
/*
    console.log(dot['dot_data'].id_transport)
    console.log(dot['dot_data'].trip_id)
    
    trans = dot['dot_data'].id_transport;
    tripid = dot['dot_data'].trip_id;
*/
   
    if(dot_infowindow != null)
      dot_infowindow.close();
    dot_infowindow = new google.maps.InfoWindow({
      content: msg
    })
    dot_infowindow.setPosition(p);
    dot_infowindow.open(map);
  });          
  this.map_bounds.extend(p);
 }
/*

*/
}