import { NgModule } from '@angular/core';

import { MapsComponent } from '../maps-track/components/maps/maps.component';

import { AgmCoreModule } from '@agm/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';

import { ModuleWithProviders } from '@angular/compiler/src/core';

@NgModule({
  declarations: [MapsComponent],
  imports: [
    AgmCoreModule,
    BrowserModule,
    CommonModule
  ],
  bootstrap: [],
  exports: [MapsComponent]
})
export class MapsTrackModule { 
  public static forRoot(environment: any): ModuleWithProviders {
    return {
        ngModule: MapsTrackModule,
        providers: [
            {
                provide: 'env', // you can also use InjectionToken
                useValue: environment
            }
        ]
    };
  }

}
